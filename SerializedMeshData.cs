using System;
using UnityEngine;

namespace MeshFarming
{
	[Serializable]
	public class SerializedMeshData
	{
		[SerializeField]
		int[] triangles = new int[0];

		[SerializeField]
		Vector3[] vertices = new Vector3[0];

		[SerializeField]
		Vector3[] normals = new Vector3[0];

		[SerializeField]
		Vector4[] tangents = new Vector4[0];

		[SerializeField]
		Vector2[] uv1 = new Vector2[0];

		[SerializeField]
		Vector2[] uv2 = new Vector2[0];

		[SerializeField]
		Color[] colors = new Color[0];

		public MeshData ToMeshData ()
		{
			var md = new MeshData();

			md.triangles = new int[triangles.Length];
			triangles.CopyTo(md.triangles, 0);

			md.vertices = new Vector3[vertices.Length];
			vertices.CopyTo(md.vertices, 0);

			md.normals = new Vector3[normals.Length];
			normals.CopyTo(md.normals, 0);

			md.tangents = new Vector4[tangents.Length];
			tangents.CopyTo(md.tangents, 0);

			md.uv1 = new Vector2[uv1.Length];
			uv1.CopyTo(md.uv1, 0);

			md.uv2 = new Vector2[uv2.Length];
			uv2.CopyTo(md.uv2, 0);

			md.colors = new Color[colors.Length];
			colors.CopyTo(md.colors, 0);

			return md;
		}

		public void FromMeshData (MeshData md)
		{
			if(triangles.Length != md.triangles.Length)
			{
				triangles = new int[md.triangles.Length];
			}
			md.triangles.CopyTo(triangles,0);

			if(vertices.Length != md.vertices.Length)
			{
				vertices = new Vector3[md.vertices.Length];
			}
			md.vertices.CopyTo(vertices,0);

			if(md.normals == null)
			{
				normals = new Vector3[0];
			}
			else
			{
				if(normals.Length != md.normals.Length)
				{
					normals = new Vector3[md.normals.Length];
				}
				md.normals.CopyTo(normals,0);
			}

			if(md.tangents == null)
			{
				tangents = new Vector4[0];
			}
			else 
			{
				if(tangents.Length != md.tangents.Length)
				{
					tangents = new Vector4[md.tangents.Length];
				}
				md.tangents.CopyTo(tangents,0);
			}

			if(md.colors == null)
			{
				colors = new Color[0];
			}
			else 
			{
				if(colors.Length != md.colors.Length)
				{
					colors = new Color[md.colors.Length];
				}
				md.colors.CopyTo(colors,0);
			}

			if(md.uv1 == null)
			{
				uv1 = new Vector2[0];
			}
			else 
			{
				if(uv1.Length != md.uv1.Length)
				{
					uv1 = new Vector2[md.uv1.Length];
				}
				md.uv1.CopyTo(vertices,0);
			}

			if(md.uv2 == null)
			{
				uv2 = new Vector2[0];
			}
			else
			{
				if(uv2.Length != md.uv2.Length)
				{
					uv2 = new Vector2[md.uv2.Length];
				}
				md.uv2.CopyTo(uv2,0);
			}
		}
	}
}

