using UnityEngine;
using System.Collections.Generic;
using System;

namespace MeshFarming
{
	public class MeshFarm
	{	
		public class MeshBuilderInfo
		{
			public GameObject gameObject;
			public MeshRenderer meshRenderer;
			public MeshBuilder builder;
		}
		
		public Bounds bounds = new Bounds();
		public Action<GameObject> onCreateGameObject;
		
		Transform _parent;
		public Transform parent
		{
			get{ return _parent; }
			set{
				if(value != _parent)
				{
					_parent = value;
					foreach(var kvp in meshBuilders)
					{
						kvp.Value.gameObject.transform.parent = _parent;
					}
				}
			}
		}
		public Dictionary<MeshAttributes, MeshBuilderInfo> meshBuilders = new Dictionary<MeshAttributes, MeshBuilderInfo>();
		public Dictionary<MeshBuilder, MeshAttributes> builderAttributes = new Dictionary<MeshBuilder, MeshAttributes>();
		List<Material> instanceMaterials = new List<Material>();

		public MeshBuilder GetMeshBuilder (Material material, int layer)
		{
			var md = new MeshAttributes(){
				material = material, 
				layer = layer
			};
			MeshBuilderInfo mbi = null;
			if(meshBuilders.TryGetValue(md, out mbi)) return mbi.builder;
			
			mbi = new MeshBuilderInfo();
			mbi.builder = new MeshBuilder();
			CreateGameObject(mbi, md);
			
			meshBuilders.Add(md, mbi);
			builderAttributes.Add(mbi.builder, md);
			
			return mbi.builder;
		}
		
		public MeshAttributes GetMeshAttributes (MeshBuilder builder)
		{
			MeshAttributes attributes;
			if(!builderAttributes.TryGetValue(builder, out attributes)) return new MeshAttributes();
			return attributes;
		}
		
		GameObject CreateGameObject (MeshBuilderInfo mbi, MeshAttributes data)
		{
			string name = "Material("+ (data.material?data.material.name:"Null")+") Layer(" + data.layer + ")";
			
			var go = new GameObject(name);
			go.layer = data.layer;
			
			go.transform.parent = parent;
				
			var mf = go.AddComponent<MeshFilter>();
			mf.sharedMesh = mbi.builder.mesh;
				
			mbi.meshRenderer = go.AddComponent<MeshRenderer>();
			mbi.meshRenderer.sharedMaterial = GameObject.Instantiate(data.material) as Material;
			instanceMaterials.Add(mbi.meshRenderer.sharedMaterial);

			if(onCreateGameObject != null)
				onCreateGameObject(go);
			
			return go;
		}
		
		public void Unload ()
		{
			foreach(var mat in instanceMaterials)
			{
				GameObject.DestroyImmediate(mat);
			}
			instanceMaterials.Clear();

			foreach(var kvp in meshBuilders)
			{
				kvp.Value.builder.Dispose();
				GameObject.DestroyImmediate(kvp.Value.gameObject);
			}
			meshBuilders.Clear();
		}
		
		public void Apply ()
		{
			bool first = false;
			foreach(var kvp in meshBuilders)
			{
				kvp.Value.builder.Apply();
				var b = kvp.Value.builder.mesh.bounds;
				if(first){
					bounds = b;
					first = true;
				}
				b.Encapsulate(b);
			}
		}
	}
}
