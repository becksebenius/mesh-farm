using UnityEngine;

namespace MeshFarming
{
	public struct MeshAttributes
	{
		public Material material;
		public int layer;
		public override bool Equals (object obj)
		{
			var md = (MeshAttributes)obj;
			return md.material == material && md.layer == layer;
		}
		public override int GetHashCode ()
		{
			return layer;
		}
	}
}