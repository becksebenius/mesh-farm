using UnityEngine;

namespace MeshFarming
{
	public class MeshBuffer
	{
		public int index;
		
		public int vertCount;
		public int vertId;
		
		public int triCount;
		public int triId;
		
		public MeshBuilder builder;
		
		public void Set (MeshData data)
		{
			triangles = data.triangles;
			vertices = data.vertices;
			if(data.normals != null) normals = data.normals;
			if(data.tangents != null) tangents = data.tangents;
			if(data.uv1 != null) uv1 = data.uv1;
			if(data.uv2 != null) uv2 = data.uv2;
			if(data.colors != null) colors = data.colors;
		}
		
		public void Clear ()
		{
			ClearTriangles();
			ClearVertices();
			if(normals != null) normals = new Vector3[vertCount];
			if(tangents != null) tangents = new Vector4[vertCount];
			if(colors != null) colors = new Color[vertCount];
			if(uv1 != null) uv1 = new Vector2[vertCount];
			if(uv2 != null) uv2 = new Vector2[vertCount];
		}
		
		public void ClearTriangles ()
		{
			triangles = new int[triCount];
		}
		public void ClearVertices ()
		{
			vertices = new Vector3[vertCount];
		}
		
		public void MarkAllDirty ()
		{
			if(IsClean) builder.MarkDirty(this);
			trianglesDirty = true;
			verticesDirty = true;
			normalsDirty = true;
			tangentsDirty = true;
			colorsDirty = true;
			uv1Dirty = true;
			uv2Dirty = true;
		}
		
		public bool trianglesDirty = false;
		int[] _triangles;
		public int[] triangles {
			get{ return _triangles; }
			set{
				if(IsClean) builder.MarkDirty(this);
				trianglesDirty = true;
				_triangles = value;
			}
		}
		
		public bool verticesDirty = false;
		Vector3[] _vertices;
		public Vector3[] vertices {
			get{ return _vertices; }
			set{
				if(IsClean) builder.MarkDirty(this);
				verticesDirty = true;
				_vertices = value;
			}
		}
		
		public bool normalsDirty = false;
		Vector3[] _normals;
		public Vector3[] normals {
			get{ return _normals; }
			set{
				if(IsClean) builder.MarkDirty(this);
				normalsDirty = true;
				_normals = value;
			}
		}
		
		public bool tangentsDirty = false;
		Vector4[] _tangents;
		public Vector4[] tangents {
			get{ return _tangents; }
			set{
				if(IsClean) builder.MarkDirty(this);
				tangentsDirty = true;
				_tangents = value;
			}
		}
		
		public bool colorsDirty = false;
		Color[] _colors;
		public Color[] colors {
			get{ return _colors; }
			set{
				if(IsClean) builder.MarkDirty(this);
				colorsDirty = true;
				_colors = value;
			}
		}
		
		public bool uv1Dirty = false;
		Vector2[] _uv1;
		public Vector2[] uv1 {
			get{ return _uv1; }
			set{
				if(IsClean) builder.MarkDirty(this);
				uv1Dirty = true;
				_uv1 = value;
			}
		}
		
		public bool uv2Dirty = false;
		Vector2[] _uv2;
		public Vector2[] uv2 {
			get{ return _uv2; }
			set{
				if(IsClean) builder.MarkDirty(this);
				uv2Dirty = true;
				_uv2 = value;
			}
		}
		
					
		bool IsClean {
			get{
				return
				  	!trianglesDirty
				&&	!verticesDirty
				&&	!normalsDirty
				&&	!tangentsDirty
				&&	!colorsDirty
				&&	!uv1Dirty
				&&	!uv2Dirty;
			}
		}
		
	}
}