using UnityEngine;
using System.Collections.Generic;

namespace MeshFarming
{
	public class MeshCompiler
	{
		public List<int> triangles = new List<int>();
		public List<Vector3> vertices = new List<Vector3>();
		public List<Vector3> normals;
		public List<Vector4> tangents;
		public List<Vector2> uv1;
		public List<Vector2> uv2;
		public List<Color> colors;
		
		public void Add (MeshData meshData)
		{
			Add(meshData.triangles, meshData.vertices, meshData.normals, 
				meshData.tangents, meshData.uv1, meshData.uv2, meshData.colors);
		}
		public void Add (
			int[] triangles, 
			Vector3[] vertices,
			Vector3[] normals, 
			Vector4[] tangents,
			Vector2[] uv1, 
			Vector2[] uv2,
			Color[] colors)
		{
			
			for(int i = 0; i < triangles.Length; i++)
				this.triangles.Add(triangles[i]+this.vertices.Count);
			
			if(normals != null && normals.Length == vertices.Length)
			{
				if(this.normals == null) this.normals = new List<Vector3>(this.vertices.Count);
				this.normals.AddRange(normals);
			}
			
			if(tangents != null && tangents.Length == vertices.Length)
			{
				if(this.tangents == null) this.tangents = new List<Vector4>(this.vertices.Count);
				this.tangents.AddRange(tangents);
			}
			
			if(uv1 != null && uv1.Length == vertices.Length)
			{
				if(this.uv1 == null) this.uv1 = new List<Vector2>(this.vertices.Count);
				this.uv1.AddRange(uv1);
			}
			
			if(uv2 != null && uv2.Length == vertices.Length)
			{
				if(this.uv2 == null) this.uv2 = new List<Vector2>(this.vertices.Count);
				this.uv2.AddRange(uv2);
			}
			
			if(colors != null && colors.Length == vertices.Length)
			{
				if(this.colors == null) this.colors = new List<Color>(this.vertices.Count);
				this.colors.AddRange(colors);
			}
			
			this.vertices.AddRange(vertices);
		}
		
		public MeshData ToMeshData ()
		{
			var md = new MeshData(triangles.ToArray(), vertices.ToArray());
			if(normals != null) md.normals = normals.ToArray();
			if(tangents != null) md.tangents = tangents.ToArray();
			if(uv1 != null) md.uv1 = uv1.ToArray();
			if(uv2 != null) md.uv2 = uv2.ToArray();
			if(colors != null) md.colors = colors.ToArray();
			return md;
		}
	}
}